import Axios from 'axios'

const axios = Axios.create({
    baseURL: 'https://api.fanpag.com/api/'
})

export default axios