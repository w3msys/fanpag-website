import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'

Vue.use(Router)

const router =  new Router({
  routes
})

router.afterEach((to, from) => {
  let [title] = document.getElementsByTagName('title')
  title.innerText = to.meta.title
  $('.f-mobile-nav').fadeOut()

  if (to.name !== 'article') {
    $('.f-header').removeClass('no_change')
  }
})

export default router
