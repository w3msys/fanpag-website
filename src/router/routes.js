import {FHome, FAbout, FWorks, FArticle, FContactUs} from './../components'

export default [
    {
        path: '/',
        name: 'home',
        meta: { title: 'Welcome to Fanpag' },
        component: FHome
    },
    {
        name: 'about',
        path: '/about',
        component: FAbout,
        meta: {title: 'Who we are'}
    },
    {
        name: 'works',
        path: '/our-works',
        meta: {title: 'See our works'},
        component: FWorks
    },
    {
        name: 'article',
        path: '/article',
        component: FArticle,
        meta: {title: 'What we do'}
    },
    {
        name: 'contact-us',
        path: '/contact-us',
        meta: {title: 'Contact Us'},
        component: FContactUs
    }
]

