import header from './commons/header.vue';
import footer from './commons/footer.vue';
import mobileNav from './commons/mobile_nav.vue';
import about from './about.vue';
import home from './home.vue';
import works from './works.vue';
import article from './article.vue';
import contact_us from './contact_us.vue'

export const FHeader = header;
export const FFooter = footer;
export const FMobileNav = mobileNav;
export const FHome = home;
export const FAbout = about;
export const FWorks = works;
export const FArticle = article;
export const FContactUs = contact_us;