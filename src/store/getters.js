export default {
    getArticles (state) {
        return state.articles
    },
    articlesDownloaded (state) {
        return state.articlesDownloaded;
    }
}