export default {
    setArticles ({commit}, articles) {
        commit('SET_ARTICLES', articles)
    }
}