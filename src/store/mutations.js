export default {
    SET_ARTICLES (state, articles) {
        state.articles.data = articles
        state.articlesDownloaded = true
    }
}