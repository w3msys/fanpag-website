$(document).ready(function (event) {
    $(window).on('scroll', function (event) {
       var moved = $(window).scrollTop()
       if (moved > 100) {
           $('.f-header').css({background: '#037a36'})
       } else {
           $('.f-header').css({background: 'transparent'})
       }
    })
})